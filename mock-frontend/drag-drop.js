/**
 * allowDrop() called when an
 * element is drag over another
 * element 
 *
 * @param <DragEvent> ev
 * @return <Void>
 */
function allowDrop(ev) {
    ev.preventDefault();
}

/**
 * drag() called when an
 * element is dragged 
 *
 * @param <DragEvent> ev
 * @return <Void>
 */
function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

/**
 * allowDrop() called when an
 * element is drop on other
 * element 
 *
 * @param <DragEvent> ev
 * @return <Void>
 */
function drop(ev) {

    //Mock of the frontend data
    var mockData = {
        "isMock": true,
        "isExample": true,
        "isRealData": false
    };

    //Next 3 lines are from the Drag and Drop function
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));

    /**
     * Dispatch Event to inform backend
     * that a change has been made in
     * the data
     */
    var onDataChange = new CustomEvent("OnDataChange", {
        detail: mockData
    });
    /**
     * Dispatch (trigger) an event when
     * the data changes on the frontend
     */
    document.dispatchEvent(onDataChange);
}

//Adding the event handler to the window
document.addEventListener("OnDataUpdate", function (event) {
    handlerOnDataUpdate(event);
});

/**
 * handlerOnDataUpdate() called
 * when the peer receives an
 * updated data from other peer
 * whom modified it
 * 
 * @param <Object> event
 * @return <Void>
 * 
 * TODO: Update schedule view
 * with the updated data
 */
function handlerOnDataUpdate(event) {

    //Data updated
    var dataUpdated = event.detail;

    console.info("Another peer changed the data, the frontend should be refreshed");
}