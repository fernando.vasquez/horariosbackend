//Adding the event handler to the window
document.addEventListener("OnDataChange", function (event) {

    //Calling the function handler
    handlerOnDataChange(event)
});

/**
 * handlerOnDataChange() called
 * when the data changes on the
 * frontend
 * 
 * @param <Object> event
 * @return <Void>
 * 
 * TODO: Send dataModified to all
 * peers in the network
 */
function handlerOnDataChange(event) {

    //New Data already modified
    var dataModified = event.detail;

    console.info("The data has been modified, other peers will be notified");
}

//Function for testing purposes
function dataIsComing(mockData) {

    /**
     * Dispatch Event to inform the frontend
     * that the data from other peer has been
     * modified
     */
    var onDataUpdate = new CustomEvent("OnDataUpdate", {
        detail: mockData
    });

    /**
     * Dispatch an event when an updated
     * data is received
     */
    document.dispatchEvent(onDataUpdate);
}